const csv = require('./writer_csv');

const args = process.argv.slice(2);
const file = args[0];
const prefix = args[1];

csv.writeNodes(file, prefix);

//node run_csv.js *input file* *output prefix*
//write json with boundaries and zero point