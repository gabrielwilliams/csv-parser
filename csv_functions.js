/**
Regular functions for use by csv parser
Adaptive City Interface Beta 2021
Gabriel Williams
inqstudio.com
**/

const fs = require("fs");
const pi = 3.1415926535;

module.exports = {

	getMeta: function (prefix, type) {
		let adpFile = prefix + "_" + type + '.json';
		let file = JSON.parse(fs.readFileSync('_csv/' + adpFile, 'utf8'));
		return file[type];
	},

    // from array of javascript objects
    genCSV: function (array) { 
        var keys = Object.keys(array[0]); // Use first element to choose the keys and the order
        var result = keys.join(",") + "\n"; // Build header

        array.forEach(function(obj) { // Add the rows
            result += keys.map(k => obj[k]).join(",") + "\n";
        });
    
        return result;
    },

	writeCSV: async function (array, name) {
		let filename = "_csv/" + name + ".csv";
		fs.writeFileSync(filename, array, 'utf8', function (err) {
			if (err) {
				console.log ("There was an issue")
			}else{
                console.log(name + ".csv" + " has been written");
            }
		});
	},

	genContractorProfile: function (permit) { // LA Contractors
        let address = permit["Address Start"] + " " +
                      permit["Street Direction"] + " " +
                      permit["Street Name"] + " " +
                      permit["Street Suffix"] + " " +
                      permit["Zip Code"];

        console.log(permit["Contractor's Business Name"]);
        console.log(address);
        console.log(permit["Work Description"]);
    },

	genContractor: function (permit, desc) { // LA Contractors

        return record = {
            Address: address,
            State: permit["Contractor State"],
            Business: RF.removeChars(permit["Contractor's Business Name"]),
            DescriptionOfWork: RF.removeChars(desc),
            PermitCount: 1,
            StatusDate: permit["Status Date"]
        }
	},

	//coordinate 
    genLAObject: function (permit, coord) {
        let address = permit["Address Start"] + " " +
                      permit["Street Direction"] + " " +
                      permit["Street Name"] + " " +
                      permit["Street Suffix"] + " " +
                      permit["Zip Code"];

        let bman = [ -118.41507, 34.00057 ]
        let distance = RF.pointDistance(bman[0], bman[1], coord[0], coord[1]);

        return record = {
            PermitNumber: permit["PCIS Permit #"],
            Location: address,
            RecordStatus: permit.Status,
            StatusDate: permit["Status Date"],
            DescriptionOfWork: RF.removeCommas(permit["Work Description"]),
            Distance: distance,
            Zone: permit["Zone"],
            FloorAreaZoning: permit["Floor Area-L.A. Zoning Code Definition"],
            FloorAreaBldgCode: permit["Floor Area-L.A. Building Code Definition"]
        }
    },

    filterByBoundary: function (input, boundary) {
        fs.readFile("_csv/" + input + ".csv", function (err, fileData) {
			parse(fileData, {columns: true, trim: true}, function(err, rows) {
                let filteredPermits = [];
                for (i=0; i < rows.length; i++) { 
                    let permit = rows[i];
                    let coordinate = extractCoordinates(permit["Latitude/Longitude"]);
                    if (RF.pointInPolygon(coordinate, boundary) &&
                        permit["Permit Type"] === "Bldg-New" &&
                        permit["Permit Sub-Type"] === "Apartment" &&
                        permit["Work Description"].indexOf("laundry")=== -1 ) {
                        let record = genLAObject(permit, coordinate);
                        filteredPermits.push(record);
                    }
                }

                let ordered = filteredPermits.sort((a,b) => { return a.Distance - b.Distance });
                let array = genCSV(ordered);
                RF.writeCSV(array, output);
            });
        });
    },

    // specifically for Los Angeles permit coordinates in LAT / LON format
	//(34.05474, -118.42628)
	extractCoordinates: function (string) {	
		// string = string.replace(/[()]/g, ");
		//string = string.replace(/[^\d.-]+/i, ");
		let shape = string.split(", ");
        let lon = parseFloat(shape[1]);
        let lat = parseFloat(shape[0]);
        let point  = [lon, lat];

        return point;
    },

	underground: function (input) {
		fs.readFile("_csv/" + input + ".csv", function (err, fileData) {
			parse(fileData, {columns: true, trim: true}, function(err, rows) {
                // data is array of objects passed to this callback as rows.
                let count = 0;
                let uniqueAddress = [];
                let filteredPermits = [];
                for (i=0; i < rows.length; i++) {                 
                    let permit = rows[i];
                    let desc = permit.DescriptionOfWork;
                    let loc = permit.Location;
                    if ( desc.includes("parking") || 
                            desc.includes("underground") ||
                            desc.includes("subterranean")) {
                        if (uniqueString(uniqueAddress, loc)) {
                            uniqueAddress.push(loc)
                            let record = genObject(permit, desc, loc);
                            filteredPermits.push(record);
                            count += 1;
                        };
                    };
                }
                //console.log("There are " + count + " unique Culver City permits containing the word "new" or "add" and "parking" between 2017 and 2019.");
                console.log("Gabriel Williams, inqstudio")
                console.log(count);
                let array = genCSV(filteredPermits);
                RF.writeCSV(array, output);
            });
        });
    },

    cleanFull: function (record) {

        const hasLineBreak = (str) => str.includes("\n");
        const hasComma = (str) => str.includes(",") || str.includes('"');
        const clean = (str) => this.removeLineBreaks(this.removeCommasQuotes(str));

        let cleanRecord = Object.assign(record);
        for (key in record) {
            let val = record[key];
            if (hasComma(val) || hasLineBreak(val)) cleanRecord[key] = clean(val);
        }

        return cleanRecord;
    },

    hasLineBreak: function (string) {
        return string.includes("\n");
    },

    hasComma: function (string) {
        return string.includes(",");
    },

    isFloat: function (n) { return ( Number(n) === n && n % 1 !== 0 ); },

    round: function (number, place) {
		return parseFloat(parseFloat(number).toFixed(place));
	},

	comma: function (num) {
		return num.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,');
	},

    removeCommasQuotes: function (string) {
		return string.replace(/[,"]/g, '');
	},

	removeCommas: function (string) {
		return string.replace(/[,]/g, '');
	},

	removeLineBreaks: function (string) {
		return string.replace(/[/\r?\n|\r/]/gm, ' ')
	},

	removeChars: function (string) {
		return string.replace(/[/\r?\n|\r/,]/gm, ' ')
	},
}
