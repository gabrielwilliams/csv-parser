/**
Adaptive City Interface Beta 2021
Gabriel Williams
inqstudio.com
**/

var csv_writer = {};
const fs = require("fs");
const parse = require("csv-parse");
const CF = require("./csv_functions");
//const RF = require("./util/regular_functions");

csv_writer.writeNodes = function (input, output) {
    
	function main() {
        console.log(input, output);
        // getHeaders();
        // getUniqueValues('Permit Class');
        // getFrequency('Permit Class');
        // FilterByYearMixed('Calendar Year Issued', 2015);
        getContractors();
        // getDescriptions()
    };

    // Work Class
    // R- 101 Single Family Houses
    // R- 102 Secondary Apartment
    // R- 103 Two Family Bldgs
    // R- 328 Resident Other Nonresident Bldg
    // C- 101 Single Family Houses
    // C- 103 Two Family Bldgs
    // C- 104 Three & Four Family Bldgs
    // C- 105 Five or More Family Bldg
    // C- 106 Mixed Use
    // C- 324 Office Bank & Professional Bldgs
    // C- 328 Commercial Other Nonresident Bld
    // Residential
    // Commercial
    
    // node run_csv Austin_Permits Austin_2016-21
    // node run_csv Austin_Permits Austin_MixedUse
    // node run_csv Austin_Permits Austin_NotSingle
    // node run_csv Austin_2016-21
    // node run_csv Austin_2016-21 recentContractors
    // node run_csv Austin_MixedUse MixedUseContractors
    // node run_csv Austin_NotSingle NotSingleContractors

    function getHeaders() {
        const permits = [];
        fs.createReadStream("_csv/" + input + ".csv")
            .pipe(parse({delimiter: ',', to: 1, columns:true }))
            .on('data', (row) => {
                permits.push(row);
            })
            .on('end', function() {
                console.log(permits);
            })
    }

    function getUniqueValues(label) {
        const values = [];
        fs.createReadStream("_csv/" + input + ".csv")
            .pipe(parse({delimiter: ',', columns: true}))
            .on('data', function(row) {
                let val = row[label];
                if (values.indexOf(val) === -1) values.push(val);
            })
            .on('error', function(err, row) {
                console.log(err, row);
            })
            .on('end', function() {
                console.log(values);
            })
    }

    function getFrequency(label) {
        const histogram = {};
        fs.createReadStream("_csv/" + input + ".csv")
            .pipe(parse({delimiter: ',', columns: true}))
            .on('data', function(row) {
                let value = row[label];
                if (value in histogram) {
                    histogram[value]++;
                }else{
                    histogram[value] = 1;
                }
            })
            .on('end', function() {
                console.log(histogram)
            })
    }

    function getContractors() {

        const genContractor = (row) => {
            return {
                company: row['Contractor Company Name'],
                contact: row['Contractor Full Name'],
                phone: row['Contractor Phone'],
                count: 1
            }
        }

        const contractors = [];

        fs.createReadStream("_csv/" + input + ".csv")
            .pipe(parse({delimiter: ',', columns: true}))
            .on('data', function(row) {
                let company = row['Contractor Company Name'];
                let contact = row['Contractor Full Name'];
                let search = (profile) => 
                    profile.company === company && profile.contact === contact;
                let index = contractors.findIndex(search);

                if (index > -1) {
                    contractors[index].count += 1;
                }else{
                    let profile = genContractor(row);
                    contractors.push(profile)
                }
            })
            .on('end', function() {
                let sorted = contractors.sort((a,b) => { return b.count - a.count });
                let result = CF.genCSV(sorted);
                CF.writeCSV(result, output)
            })
    }

    function getDescriptions() {
        fs.createReadStream("_csv/" + input + ".csv")
            .pipe(parse({delimiter: ',', columns: true, ltrim: true}))
            .on('data', function(row) {

                let work = row['Permit Class'];
                if (work.includes('Other Than')) console.log(row['Description']);

            })
            .on('end', function() {
                console.log("Descriptions logged");
            })
    }

    function FilterByYear(label, year) {
        const permits = [];
        fs.createReadStream("_csv/" + input + ".csv")
            .pipe(parse({delimiter: ',', columns: true, ltrim: true}))
            .on('data', function(row) {
                let work = row['Work Class'];
                let type = row['Permit Type'];
                let license = row['Permit Class']
                let issued = parseInt(row[label]);
                if (issued > year && 
                    type === 'BP' && 
                    work === 'New' &&
                    !license.includes('Single') &&
                    !license.includes('Other Than') &&
                    !license.includes('Boat') &&
                    !license.includes('Cabins') &&
                    !license.includes('Garage') &&
                    !license.includes('Amusement') &&
                    !license.includes('Remodel') &&
                    !license.includes('Accessory') &&
                    !license.includes('Industrial') &&
                    !license.includes('Addition') &&
                    !license.includes('Retaining') &&
                    !license.includes('Religious') &&
                    !license.includes('Educational') &&
                    !license.includes('Public') &&
                    !license.includes('Institutional') &&
                    !license.includes('Shelter')) {
                    let obj = CF.cleanFull(row);
                    permits.push(obj);
                }
            })
            .on('end', function() {
                let result = CF.genCSV(permits);
                CF.writeCSV(result, output)
            })
    }

    function FilterByYearMixed(label, year) {
        const permits = [];
        fs.createReadStream("_csv/" + input + ".csv")
            .pipe(parse({delimiter: ',', columns: true, ltrim: true}))
            .on('data', function(row) {
                let work = row['Work Class'];
                let type = row['Permit Type'];
                let license = row['Permit Class']
                let issued = parseInt(row[label]);
                if (issued > year && 
                    type === 'BP' && 
                    work === 'New' &&
                    license.includes('Family') &&
                    !license.includes('Single'))  { // More Family
                    let obj = CF.cleanFull(row);
                    permits.push(obj);
                }
            })
            .on('end', function() {
                let result = CF.genCSV(permits);
                CF.writeCSV(result, output)
            })
    }

	main(input, output);
};

if (typeof module !== "undefined") module.exports = csv_writer;